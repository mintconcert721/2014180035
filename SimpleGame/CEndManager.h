#pragma once

#include "Global.h"
#include "Renderer.h"

class CEndManager
{
private:
	int* m_GameState;
	Renderer *m_Renderer = NULL;

	// Texture Image
	GLuint m_EndTexture;
	GLuint m_ChoiceTexture;
	bool isExit = false;

public:
	CEndManager();
	~CEndManager();

public:
	// state ��������
	void SetGameState(int* state);

	void RenderScene(float elapsedTime);
	void Update(float elapsedTime);
	void Input(unsigned char input);

	void ManagerDefault();
};

