#pragma once
#include "CSceneManager.h"
#include "CIntroManager.h"
#include "CEndManager.h"

class CGameManager
{
private:
	int m_GameState = 0; // 0 : 인트로, 1 : 게임플레이, 2 : 선택, 3: 종료
	int m_prevGameState;

	CIntroManager* m_IntroManager = NULL;
	CPlayManager* m_PlayeManager = NULL;
	CEndManager* m_EndManager = NULL;

public:
	CGameManager();
	~CGameManager();

public:
	void RenderScene(float elapsedTime);
	void Update(float elapsedTime);

	void ApplyForce(float x, float y, float z, float elapsedTime);
	void Shoot(int shootDirection);
	void KeyDownInput(unsigned char key, int x, int y);

	void SetGameState(int state);
	void GetGameState(int* state);

	void ManagerDefault();
};

