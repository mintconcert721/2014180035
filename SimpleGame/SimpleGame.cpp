/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include <Windows.h>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"
#include "CGameManager.h"

CGameManager* g_GameManager = NULL;

DWORD g_PrevTime = 0;

bool g_KeyW = false;
bool g_KeyA = false;
bool g_KeyS = false;
bool g_KeyD = false;
bool g_KeySP = false;

int g_Shoot = SHOOT_NONE;

void RenderScene(void)
{
	if (g_PrevTime == 0) g_PrevTime = timeGetTime();

	DWORD curTime = timeGetTime();
	DWORD elapsedTime = curTime - g_PrevTime;
	g_PrevTime = curTime;

	float fElapsedTime = (float)elapsedTime / 1000.f;
	float fx = 0.0f;
	float fy = 0.0f;
	float fz = 0.0f;
	float amount = 5.0f;
	float jumpamount = 25.0f;

	if (g_KeyS) fy -= amount;
	if (g_KeyW) fy += amount;
	if (g_KeyA) fx -= amount;	
	if (g_KeyD) fx += amount;
	if (g_KeySP) fz += jumpamount;

	g_GameManager->Update(fElapsedTime);
	g_GameManager->ApplyForce(fx, fy, fz, fElapsedTime);
	g_GameManager->RenderScene(fElapsedTime);
	g_GameManager->Shoot(g_Shoot);

	glutSwapBuffers();
}

void Idle(void)
{
	RenderScene();
}

void MouseInput(int button, int state, int x, int y)
{
	RenderScene();
}

void KeyDownInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W') g_KeyW = true;
	else if (key == 'a' || key == 'A') g_KeyA = true;
	else if (key == 's' || key == 'S') g_KeyS = true;
	else if (key == 'd' || key == 'D') g_KeyD = true;
	else if (key == 'q' || key == 'Q') exit(1);
	else if (key == ' ') g_KeySP = true;

	g_GameManager->KeyDownInput(key, x, y);
}

void KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W') g_KeyW = false;
	else if (key == 'a' || key == 'A') g_KeyA = false;
	else if (key == 's' || key == 'S') g_KeyS = false;
	else if (key == 'd' || key == 'D') g_KeyD = false;
	else if (key == ' ')	g_KeySP = false;
}

void SpecialKeyDownInput(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		g_Shoot = SHOOT_UP;
		break;
	case GLUT_KEY_DOWN:
		g_Shoot = SHOOT_DOWN;
		break;
	case GLUT_KEY_LEFT:
		g_Shoot = SHOOT_LEFT;
		break;
	case GLUT_KEY_RIGHT:
		g_Shoot = SHOOT_RIGHT;
		break;
	}
}

void SpecialKeyUpInput(int key, int x, int y)
{
	g_Shoot = SHOOT_NONE;
}

int main(int argc, char **argv)
{
	// Initialize Random Seed Value
	srand(unsigned int(time(NULL)));

	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutCreateWindow("Game Software Engineering KPU");

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}

	// Initialize Renderer
	g_GameManager = new CGameManager();

	glutDisplayFunc(RenderScene);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput); // 키보드가 눌렸을 때 호출되는 함수
	glutKeyboardUpFunc(KeyUpInput); // 키보드가 뗴어졌을 때 호출되는 함수

	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF); // 반복되는 것이 없어짐

	glutMouseFunc(MouseInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);

	glutMainLoop();

	delete g_GameManager;

	return 0;
}

