#include "stdafx.h"
#include "CEndManager.h"


CEndManager::CEndManager()
{
	m_Renderer = new Renderer(WINDOW_WIDTH, WINDOW_HEIGHT);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	m_ChoiceTexture = m_Renderer->CreatePngTexture(DETERMINE_IMAGE);
	m_EndTexture = m_Renderer->CreatePngTexture(FINISH_IMAGE);
}


CEndManager::~CEndManager()
{
	delete m_Renderer;
}


void CEndManager::SetGameState(int* state)
{
	m_GameState = state;
}

float g_finish_time = 0.0f;
void CEndManager::RenderScene(float elapsedTime)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	switch (*m_GameState)
	{
	case 2:
		m_Renderer->DrawTextureRectDepth(0.0f, 0.0f, 0.0f, WINDOW_WIDTH, WINDOW_HEIGHT, 1.0f, 1.0f, 1.0f, 1.0f, m_ChoiceTexture, 1.0f);
		break;
	case 3:
		m_Renderer->DrawTextureRectDepth(0.0f, 0.0f, 0.0f, WINDOW_WIDTH, WINDOW_HEIGHT, 1.0f, 1.0f, 1.0f, 1.0f, m_EndTexture, 1.0f);
		isExit = true;
		break;
	}

	if (isExit == true)
		g_finish_time += 0.01f;
}

void CEndManager::Update(float elapsedTime)
{
	if (g_finish_time > 50.0f)
		exit(1);
}

void CEndManager::Input(unsigned char input)
{
	if (input == '1')
	{
		*m_GameState = 0;
	}
	if (input == '2')
	{
		exit(1);
	}
}

void CEndManager::ManagerDefault()
{
}
