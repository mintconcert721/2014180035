#include "stdafx.h"
#include "CGameManager.h"


CGameManager::CGameManager()
{
	m_IntroManager = new CIntroManager();
	m_PlayeManager = new CPlayManager();
	m_EndManager = new CEndManager();

	m_IntroManager->SetGameState(&m_GameState);
	m_PlayeManager->SetGameState(&m_GameState);
	m_EndManager->SetGameState(&m_GameState);
}


CGameManager::~CGameManager()
{
	delete m_EndManager;
	delete m_PlayeManager;
	delete m_IntroManager;
}

void CGameManager::RenderScene(float elapsedTime)
{
	switch (m_GameState)
	{
	case 0:
		m_IntroManager->RenderScene(elapsedTime);
		break;
	case 1:
		m_PlayeManager->RenderScene(elapsedTime);
		break;
	case 2:
		m_EndManager->RenderScene(elapsedTime);
		break;
	case 3:
		m_EndManager->RenderScene(elapsedTime);
		break;
	}
}

void CGameManager::Update(float elapsedTime)
{
	if (m_prevGameState != m_GameState)
	{
		switch (m_GameState)
		{
		case 0:
			m_IntroManager->ManagerDefault();
			break;
		case 1:
			m_PlayeManager->ManagerDefault();
			break;
		case 2:
			m_EndManager->ManagerDefault();
			break;
		case 3:
			m_EndManager->ManagerDefault();
			break;
		}
	}

	m_prevGameState = m_GameState;

	switch (m_GameState)
	{
	case 0:
		break;
	case 1:
		m_PlayeManager->Update(elapsedTime);
		m_PlayeManager->UpdateCollision();
		m_PlayeManager->DoGarbageCollect();
		break;
	case 2:
		m_EndManager->Update(elapsedTime);
		break;
	case 3:
		m_EndManager->Update(elapsedTime);
		break;
	}
}


void CGameManager::ApplyForce(float x, float y, float z, float elapsedTime)
{
	switch (m_GameState)
	{
	case 0:
		break;
	case 1:
		m_PlayeManager->ApplyForce(x, y, z, elapsedTime);
		break;
	case 2:
		break;
	case 3:
		break;
	}
}

void CGameManager::Shoot(int shootDirection)
{
	switch (m_GameState)
	{
	case 0:
		break;
	case 1:
		m_PlayeManager->Shoot(shootDirection);
		break;
	case 2:
		break;
	case 3:
		break;
	}
}

void CGameManager::KeyDownInput(unsigned char key, int x, int y)
{
	switch (m_GameState)
	{
	case 0:
		m_IntroManager->Input(key);
		break;
	case 1:
		break;
	case 2:
		m_EndManager->Input(key);
		break;
	case 3:
		break;
	}
}


void CGameManager::SetGameState(int state)
{
	m_GameState = state;
}


void CGameManager::GetGameState(int* state)
{
	*state = m_GameState;
}

void CGameManager::ManagerDefault()
{
}