#include "stdafx.h"
#include "CIntroManager.h"


CIntroManager::CIntroManager()
{
	m_Renderer = new Renderer(WINDOW_WIDTH, WINDOW_HEIGHT);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	m_Texture = m_Renderer->CreatePngTexture(START_IMAGE);
}


CIntroManager::~CIntroManager()
{
	delete m_Renderer;
}


void CIntroManager::SetGameState(int* state)
{
	m_GameState = state;
}

void CIntroManager::RenderScene(float elapsedTime)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	m_Renderer->DrawTextureRectDepth(0.0f, 0.0f, 0.0f, WINDOW_WIDTH, WINDOW_HEIGHT, 1.0f, 1.0f, 1.0f, 1.0f, m_Texture, 1.0f);
}

void CIntroManager::Update(float elapsedTime)
{
}

void CIntroManager::Input(unsigned char input)
{
	if (input == 'p')
		*m_GameState = 1;
}

void CIntroManager::ManagerDefault()
{
}
