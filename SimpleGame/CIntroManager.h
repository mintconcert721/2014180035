#pragma once

#include "Global.h"
#include "Renderer.h"

class CIntroManager
{
private:
	int* m_GameState;
	Renderer *m_Renderer = NULL;

	// Texture Image
	GLuint m_Texture;

public:
	CIntroManager();
	~CIntroManager();

public:
	// state ��������
	void SetGameState(int* state);

	void RenderScene(float elapsedTime);
	void Update(float elapsedTime);
	void Input(unsigned char input);

	void ManagerDefault();
};

