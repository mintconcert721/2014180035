#include "stdafx.h"
#include "CSceneManager.h"
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"


size_t g_ShootCount = 0;
bool ISSAC_DAMAGED = false;
float g_BossTearElapsedTime = 0.0f;
bool BOSS_ATTACK_START = false;

CPlayManager::CPlayManager()
{
	m_Renderer = new Renderer(WINDOW_WIDTH, WINDOW_HEIGHT);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	// Initiate Object Array.
	for (int index = 0; index < MAX_OBJECTS; index++)
		m_Objects[index] = NULL;

	// Initiate Hero.
	m_Objects[HERO_ID] = new CObject();
	m_Objects[HERO_ID]->SetPostion(0.0f, 0.0f, 0.0f);
	m_Objects[HERO_ID]->SetVelocity(0.0f, 0.0f, 0.0f);
	m_Objects[HERO_ID]->SetAcceleration(0.0f, 0.0f, 0.0f);
	m_Objects[HERO_ID]->SetSize(1.0f, 1.0f, 1.0f);
	m_Objects[HERO_ID]->SetMass(1.0f);
	m_Objects[HERO_ID]->SetFrictionCeof(0.3f);
	m_Objects[HERO_ID]->SetColor(1.0f, 1.0f, 1.0f, 1.0f);
	m_Objects[HERO_ID]->SetKind(KIND_HERO);
	m_Objects[HERO_ID]->SetHP(6);
	m_Objects[HERO_ID]->SetState(STATE_GROUND);

	AddObject(3.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, ((float)rand() / RAND_MAX) * 2.0f + 0.6f, ((float)rand() / RAND_MAX) * 2.0f + 0.6f, 0.0f,
		KIND_MONSTER, 500000, STATE_GROUND); // Initiate Moster Object.

	m_Texture = m_Renderer->CreatePngTexture(ISAAC_IMAGE); // Create Hero Image.
	m_AttackTexture = m_Renderer->CreatePngTexture(TEAR_IMAGE); // Create Hero Attack Image.
	m_MonsterTexture = m_Renderer->CreatePngTexture(MONSTER_IMAGE); // Create Test Object Image.
	m_BackgroundTexture = m_Renderer->CreatePngTexture(BACKGROUND_IMAGE); // Create Background Image.
	m_MonsterHPTexture = m_Renderer->CreatePngTexture(MOSTER_HEALTHBAR_IMAGE);
	m_IssacHPTexture = m_Renderer->CreatePngTexture(ISSAC_HEALTHBAR_IMAGE);

	m_Sound = new Sound();

	m_SoundBG = m_Sound->CreateSound(BACKGROUND_SOUND);
	m_SoundShooting = m_Sound->CreateSound(SHOOTING_SOUND);
	m_SoundShootingCollision = m_Sound->CreateSound(COLLISION_SOUND);
	m_SoundMonsterDeath = m_Sound->CreateSound(MONSTER_DEATH_SOUND);

	m_Sound->PlaySound(m_SoundBG, true, 0.05f); // Play Background Music.
}


CPlayManager::~CPlayManager()
{
	for (int index = 0; index < MAX_OBJECTS; index++)
		DeleteObject(index);

	m_Renderer->DeleteTexture(m_Texture);
	m_Renderer->DeleteTexture(m_AttackTexture);
	m_Renderer->DeleteTexture(m_MonsterTexture);
	m_Renderer->DeleteTexture(m_BackgroundTexture);

	delete m_Sound;
	delete m_Renderer;
}


float g_time = 0.0f;
void CPlayManager::RenderScene(float elapsedTime)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	m_Renderer->DrawTextureRectDepth(0.0f, 0.0f, 0.0f, WINDOW_WIDTH, WINDOW_HEIGHT, 1.0f, 1.0f, 1.0f, 1.0f, m_BackgroundTexture, 1.0f);

	for (int index = 0; index < MAX_OBJECTS; index++)
	{
		if (m_Objects[index] != NULL)
		{
			float positionX, positionY, positionZ;
			float sizeX, sizeY, sizeZ;
			float r, g, b, a;
			int kind;

			m_Objects[index]->GetPosition(&positionX, &positionY, &positionZ);
			m_Objects[index]->GetSize(&sizeX, &sizeY, &sizeZ);
			m_Objects[index]->GetColor(&r, &g, &b, &a);
			m_Objects[index]->GetKind(&kind);

			float newX, newY, newZ, newSX, newSY, newSZ;
			newX = positionX * 100.0f;
			newY = positionY * 100.0f;
			newZ = positionZ * 100.0f;

			newSX = sizeX * 100.0f;
			newSY = sizeY * 100.0f;
			newSZ = sizeZ * 100.0f;

			switch (kind)
			{
			case KIND_HERO:
				{
					m_Renderer->DrawTextureRectHeight(newX, newY, newZ, newSX, newSY, r, g, b, a, m_Texture, newZ);
					int issacHP;
					m_Objects[HERO_ID]->GetHP(&issacHP);
					switch (issacHP)
					{
					case 1:
						m_Renderer->DrawTextureRectSeqXYDepth(-450.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 1, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-375.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 2, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-300.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 2, 0, 4, 1, 0.1f);
						break;
					case 2:
						m_Renderer->DrawTextureRectSeqXYDepth(-450.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 0, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-375.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 2, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-300.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 2, 0, 4, 1, 0.1f);
						break;
					case 3:
						m_Renderer->DrawTextureRectSeqXYDepth(-450.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 0, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-375.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 1, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-300.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 2, 0, 4, 1, 0.1f);
						break;
					case 4:
						m_Renderer->DrawTextureRectSeqXYDepth(-450.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 0, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-375.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 0, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-300.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 2, 0, 4, 1, 0.1f);
						break;
					case 5:
						m_Renderer->DrawTextureRectSeqXYDepth(-450.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 0, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-375.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 0, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-300.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 1, 0, 4, 1, 0.1f);
						break;
					case 6:
						m_Renderer->DrawTextureRectSeqXYDepth(-450.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 0, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-375.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 0, 0, 4, 1, 0.1f);
						m_Renderer->DrawTextureRectSeqXYDepth(-300.0f, 250.0f, 0.0f, 75.0f, 75.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_IssacHPTexture, 0, 0, 4, 1, 0.1f);
						break;
					}
					break;
				}
			case KIND_MONSTER:
				{
					int hp;
					m_Objects[index]->GetHP(&hp);
					float hpPercent = hp / 500000.0f;
					float hpBarX = 300.0f, hpBarY = 260.0f;
					m_Renderer->DrawTextureRect(newX, newY, newZ, newSX, newSY, r, g, b, a, m_MonsterTexture);
					m_Renderer->DrawTextureRectDepth(hpBarX, hpBarY, 0.0f, 450.0f, 100.0f, 1.0f, 1.0f, 1.0f, 1.0f, m_MonsterHPTexture, 1.0f);
					m_Renderer->DrawSolidRectGauge(hpBarX, hpBarY, 0.0f, 300.0f, 20.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, hpPercent);
					break;
				}
			case KIND_BULLET:
				m_Renderer->DrawTextureRect(newX, newY, newZ, newSX, newSY, r, g, b, a, m_AttackTexture);
				break;
			case KIND_MOSTER_BULLET:
				m_Renderer->DrawTextureRect(newX, newY, newZ, newSX, newSY, 1.0f, 0.0f, 0.0f, a, m_AttackTexture);
				break;
			}
		}
	}

	if (ISSAC_DAMAGED == true)
	{
		g_time += 0.01f;
	}
}


void CPlayManager::Update(float time)
{
	// Current Time - Previous Time = ElapsedTime
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			m_Objects[i]->Update(time);
		}
	}

	if (g_time > 15.0f)
	{
		ISSAC_DAMAGED = false;
		g_time = 0.0f;
	}

	g_BossTearElapsedTime += 0.01f;
	if (g_BossTearElapsedTime > 40.0f)
	{
		BOSS_ATTACK_START = true;
		g_BossTearElapsedTime = 0.0f;
	}
}


void CPlayManager::ApplyForce(float x, float y, float z, float time)
{
	if (m_Objects[HERO_ID] == NULL)
		return;

	int state;
	m_Objects[HERO_ID]->GetState(&state);

	if (state == STATE_AIR)
		z = 0.0f;

	float hx, hy, hz;
	m_Objects[HERO_ID]->GetPosition(&hx, &hy, &hz);

	float vx, vy, vz;
	m_Objects[HERO_ID]->GetVelocity(&vx, &vy, &vz);

	m_Objects[HERO_ID]->ApplyForce(x, y, z, time);


	if (hx < BOUNDARY_LEFT + 0.9f) // 1 0 0
	{
		vx = 0.0f;
		hx = BOUNDARY_LEFT + 0.9f;
		m_Objects[HERO_ID]->SetVelocity(vx, vy, vz);
		m_Objects[HERO_ID]->SetPostion(hx, hy, hz);
	}
	if (hx > BOUNDARY_RIGHT - 0.9f) // -1 0 0
	{
		vx = 0.0f;
		hx = BOUNDARY_RIGHT - 0.9f;
		m_Objects[HERO_ID]->SetVelocity(vx, vy, vz);
		m_Objects[HERO_ID]->SetPostion(hx, hy, hz);
	}
	if (hy < BOUNDARY_DOWN + 0.7f) // 0 1 0
	{
		vy = 0.0f;
		hy = BOUNDARY_DOWN + 0.7f;
		m_Objects[HERO_ID]->SetVelocity(vx, vy, vz);
		m_Objects[HERO_ID]->SetPostion(hx, hy, hz);
	}
	if (hy > BOUNDARY_UP - 1.2f) // 0 -1 0
	{
		vy = 0.0f;
		hy = BOUNDARY_UP - 1.2f;
		m_Objects[HERO_ID]->SetVelocity(vx, vy, vz);
		m_Objects[HERO_ID]->SetPostion(hx, hy, hz);
	}

	m_Objects[HERO_ID]->SetPostion(hx, hy, hz);

	for (int i = 1; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			int kind;
			m_Objects[i]->GetKind(&kind);
			if (kind == KIND_MONSTER)
			{
				float monsterPositionX, monsterPositionY, monsterPositionZ;
				float monsterVelocityX, monsterVelocityY, monsterVelocityZ;

				m_Objects[i]->GetPosition(&monsterPositionX, &monsterPositionY, &monsterPositionZ);
				m_Objects[i]->GetVelocity(&monsterVelocityX, &monsterVelocityY, &monsterVelocityZ);

				// HERE
				if (BOSS_ATTACK_START == true)
				{
					int bossShootNumber = 10;
					int currentShootNumber = 0;
					while (currentShootNumber < bossShootNumber)
					{
						double newVelocityX = cos(((float)(currentShootNumber * 36)) * 3.141592 / 180.0f);
						double newVelocityY = sin(((float)(currentShootNumber * 36)) * 3.141592 / 180.0f);

						AddObject(monsterPositionX, monsterPositionY, monsterPositionZ, 0.5f, 0.5f, 0.5f, (float)newVelocityX, (float)newVelocityY, monsterVelocityZ,
							KIND_MOSTER_BULLET, 10, STATE_GROUND);

						m_Sound->PlaySound(m_SoundShooting, false, 1.0f);
						currentShootNumber++;
					}
					BOSS_ATTACK_START = false;
				}

				if (monsterPositionX < BOUNDARY_LEFT + 0.9f) // 1 0 0
				{
					float wallNormalX = 1.0f, wallNormalY = 0.0f, wallNormalZ = 0.0f;

					// ���� ���� ���� : (1, 0, 0)
					// ������ �Ի� ���� : (monsterVelocityX, monsterVelocityY, monsterVelocityZ)
					// �ݻ� ���� : ������ �Ի� ���� + 2 * ���� ���� * DOT(-������ �Ի� ����, ���� ����)
					float dotProductResult = -(monsterVelocityX * wallNormalX + monsterVelocityY * wallNormalY + monsterVelocityZ * wallNormalZ);

					float reflectionX, reflectionY, reflectionZ;
					reflectionX = monsterVelocityX + 2 * wallNormalX * dotProductResult;
					reflectionY = monsterVelocityY + 2 * wallNormalY * dotProductResult;
					reflectionZ = monsterVelocityZ + 2 * wallNormalZ * dotProductResult;

					monsterPositionX = BOUNDARY_LEFT + 0.9f;

					m_Objects[i]->SetVelocity(reflectionX, reflectionY, reflectionZ);
					m_Objects[i]->SetPostion(monsterPositionX, monsterPositionY, monsterPositionZ);
				}
				if (monsterPositionX > BOUNDARY_RIGHT - 0.9f) // -1 0 0
				{
					float wallNormalX = -1.0f, wallNormalY = 0.0f, wallNormalZ = 0.0f;

					// ���� ���� ���� : (1, 0, 0)
					// ������ �Ի� ���� : (monsterVelocityX, monsterVelocityY, monsterVelocityZ)
					// �ݻ� ���� : ������ �Ի� ���� + 2 * ���� ���� * DOT(-������ �Ի� ����, ���� ����)
					float dotProductResult = -(monsterVelocityX * wallNormalX + monsterVelocityY * wallNormalY + monsterVelocityZ * wallNormalZ);

					float reflectionX, reflectionY, reflectionZ;
					reflectionX = monsterVelocityX + 2 * wallNormalX * dotProductResult;
					reflectionY = monsterVelocityY + 2 * wallNormalY * dotProductResult;
					reflectionZ = monsterVelocityZ + 2 * wallNormalZ * dotProductResult;

					monsterPositionX = BOUNDARY_RIGHT - 0.9f;

					m_Objects[i]->SetVelocity(reflectionX, reflectionY, reflectionZ);
					m_Objects[i]->SetPostion(monsterPositionX, monsterPositionY, monsterPositionZ);
				}
				if (monsterPositionY < BOUNDARY_DOWN + 1.0f) // 0 1 0
				{
					float wallNormalX = 0.0f, wallNormalY = 1.0f, wallNormalZ = 0.0f;

					// ���� ���� ���� : (1, 0, 0)
					// ������ �Ի� ���� : (monsterVelocityX, monsterVelocityY, monsterVelocityZ)
					// �ݻ� ���� : ������ �Ի� ���� + 2 * ���� ���� * DOT(-������ �Ի� ����, ���� ����)
					float dotProductResult = -(monsterVelocityX * wallNormalX + monsterVelocityY * wallNormalY + monsterVelocityZ * wallNormalZ);

					float reflectionX, reflectionY, reflectionZ;
					reflectionX = monsterVelocityX + 2 * wallNormalX * dotProductResult;
					reflectionY = monsterVelocityY + 2 * wallNormalY * dotProductResult;
					reflectionZ = monsterVelocityZ + 2 * wallNormalZ * dotProductResult;

					monsterPositionY = BOUNDARY_DOWN + 1.0f;

					m_Objects[i]->SetVelocity(reflectionX, reflectionY, reflectionZ);
					m_Objects[i]->SetPostion(monsterPositionX, monsterPositionY, monsterPositionZ);
				}
				if (monsterPositionY > BOUNDARY_UP - 1.0f) // 0 -1 0
				{
					float wallNormalX = 0.0f, wallNormalY = -1.0f, wallNormalZ = 0.0f;

					// ���� ���� ���� : (1, 0, 0)
					// ������ �Ի� ���� : (monsterVelocityX, monsterVelocityY, monsterVelocityZ)
					// �ݻ� ���� : ������ �Ի� ���� + 2 * ���� ���� * DOT(-������ �Ի� ����, ���� ����)
					float dotProductResult = -(monsterVelocityX * wallNormalX + monsterVelocityY * wallNormalY + monsterVelocityZ * wallNormalZ);

					float reflectionX, reflectionY, reflectionZ;
					reflectionX = monsterVelocityX + 2 * wallNormalX * dotProductResult;
					reflectionY = monsterVelocityY + 2 * wallNormalY * dotProductResult;
					reflectionZ = monsterVelocityZ + 2 * wallNormalZ * dotProductResult;

					monsterPositionY = BOUNDARY_UP - 1.0f;

					m_Objects[i]->SetVelocity(reflectionX, reflectionY, reflectionZ);
					m_Objects[i]->SetPostion(monsterPositionX, monsterPositionY, monsterPositionZ);
				}
			}
		}
	}
}


int CPlayManager::FindEmptySlot() // Find empty slot.
{
	int index = -1;
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
			index = i;
	}
	if (index == -1)
		std::cout << "all slots are occupied" << std::endl;
	return index;
}


void CPlayManager::Shoot(int shootDirection)
{
	if (shootDirection == SHOOT_NONE)
	{
		g_ShootCount = 0;
		return;
	}

	if (g_ShootCount > 100)
	{
		float pX, pY, pZ, vX, vY, vZ;
		m_Objects[HERO_ID]->GetPosition(&pX, &pY, &pZ);
		m_Objects[HERO_ID]->GetVelocity(&vX, &vY, &vZ);

		float bullet_velocityX, bullet_velocityY, bullet_velocityZ;
		bullet_velocityX = bullet_velocityY = bullet_velocityZ = 0.0f;

		float amount = 3.0f;

		switch (shootDirection)
		{
		case SHOOT_UP:
			bullet_velocityX = 0.0f;
			bullet_velocityY = amount;
			bullet_velocityZ = 0.0f;
			break;
		case SHOOT_DOWN:
			bullet_velocityX = 0.0f;
			bullet_velocityY = -amount;
			bullet_velocityZ = 0.0f;
			break;
		case SHOOT_LEFT:
			bullet_velocityX = -amount;
			bullet_velocityY = 0.0f;
			bullet_velocityZ = 0.0f;
			break;
		case SHOOT_RIGHT:
			bullet_velocityX = amount;
			bullet_velocityY = 0.0f;
			bullet_velocityZ = 0.0f;
			break;
		}

		bullet_velocityX += vX;
		bullet_velocityY += vY;
		bullet_velocityZ += vZ;

		AddObject(pX, pY, pZ, 0.5f, 0.5f, 0.5f, bullet_velocityX, bullet_velocityY, 0.2f, KIND_BULLET, 2500, STATE_AIR);
		m_Sound->PlaySound(m_SoundShooting, false, 1.0f);

		g_ShootCount = 0;
	}
	g_ShootCount++;
}


void CPlayManager::AddObject(float positionX, float positionY, float positionZ, float sizeX, float sizeY, float sizeZ,
	float velocityX, float velocityY, float velocityZ, int kind, int hp, int state)
{
	// Find empty slot.
	int index = FindEmptySlot();

	// Allocate
	if (index < 0)
		return;

	m_Objects[index] = new CObject();
	m_Objects[index]->SetPostion(positionX, positionY, positionZ);
	m_Objects[index]->SetVelocity(velocityX, velocityY, velocityZ);
	m_Objects[index]->SetAcceleration(0.0f, 0.0f, 0.0f);
	m_Objects[index]->SetSize(sizeX, sizeY, sizeZ);
	m_Objects[index]->SetMass(1.0f);
	if(kind == KIND_MONSTER)
		m_Objects[index]->SetFrictionCeof(0.0f);
	else if(kind == KIND_MOSTER_BULLET)
		m_Objects[index]->SetFrictionCeof(0.0f);
	else
		m_Objects[index]->SetFrictionCeof(0.1f);
	m_Objects[index]->SetColor(1.0f, 1.0f, 1.0f, 1.0f);
	m_Objects[index]->SetKind(kind);
	m_Objects[index]->SetHP(hp);
	m_Objects[index]->SetState(state);
}


void CPlayManager::DeleteObject(size_t index)
{
	if (m_Objects[index] != NULL)
	{
		delete m_Objects[index];
		m_Objects[index] = NULL; // If you delete the target, you must change it to NULL.
	}
}


bool CPlayManager::RRCollision(float minX1, float minY1, float maxX1, float maxY1, float minX2, float minY2, float maxX2, float maxY2)
{
	if (minX1 > maxX2)
		return false;
	if (maxX1 < minX2)
		return false;
	if (minY1 > maxY2)
		return false;
	if (maxY1 < minY2)
		return false;

	// Precise Collision Point Inspection Code

	return true;
}


void CPlayManager::UpdateCollision()
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
			continue;

		for (int j = i + 1; j < MAX_OBJECTS; j++)
		{
			if (m_Objects[j] == NULL)
				continue;

			if (i == j)
				continue;

			float positionX, positionY, positionZ;
			float sizeX, sizeY, sizeZ;
			int kind;
			float minX1, minY1, minZ1, maxX1, maxY1, maxZ1;
			float minX2, minY2, maxX2, maxY2, minZ2, maxZ2;

			m_Objects[i]->GetPosition(&positionX, &positionY, &positionZ);
			m_Objects[i]->GetSize(&sizeX, &sizeY, &sizeZ);
			m_Objects[i]->GetKind(&kind);

			minX1 = positionX - sizeX * 0.5f;
			maxX1 = positionX + sizeX * 0.5f;
			if (kind == KIND_HERO)
			{
				minY1 = positionY - sizeY * 0.5f;
			}
			else
			{
				minY1 = positionY - sizeY;
			}
			maxY1 = positionY + sizeY * 0.5f;
			minZ1 = positionZ - sizeZ * 0.5f;
			maxZ1 = positionZ + sizeZ * 0.5f;

			m_Objects[j]->GetPosition(&positionX, &positionY, &positionZ);
			m_Objects[j]->GetSize(&sizeX, &sizeY, &sizeZ);
			m_Objects[i]->GetKind(&kind);

			minX2 = positionX - sizeX * 0.5f;
			maxX2 = positionX + sizeX * 0.5f;
			if (kind == KIND_HERO)
			{
				minY2 = positionY - sizeY * 0.5f;
			}
			else
			{
				minY2 = positionY - sizeY;
			}
			maxY2 = positionY + sizeY * 0.5f;
			minZ2 = positionZ - sizeZ * 0.5f;
			maxZ2 = positionZ + sizeZ * 0.5f;

			// Check Collision
			if (BBCollision(minX1, minY1, minZ1, maxX1, maxY1, maxZ1, minX2, minY2, minZ2, maxX2, maxY2, maxZ2))
			{
				ProcessCollision(i, j);
			}
		}
	}
}


bool CPlayManager::BBCollision(float minX1, float minY1, float minZ1, float maxX1, float maxY1, float maxZ1, float minX2, float minY2, float minZ2, float maxX2, float maxY2, float maxZ2)
{
	if (minX1 > maxX2)
		return false;
	if (maxX1 < minX2)
		return false;
	if (minY1 > maxY2)
		return false;
	if (maxY1 < minY2)
		return false;
	if (minZ1 > maxZ2)
		return false;
	if (maxZ1 < minZ2)
		return false;

	// Precise Collision Point Inspection Code

	return true;
}


void CPlayManager::ProcessCollision(int i, int j)
{
	CObject* obj1 = m_Objects[i]; // source
	CObject* obj2 = m_Objects[j]; // destination

	int kindObj1;
	obj1->GetKind(&kindObj1);

	int kindObj2;
	obj2->GetKind(&kindObj2);

	if (kindObj1 == KIND_BULLET && kindObj2 == KIND_MONSTER) // Object1 : Bullet and Object2 : Building
	{
		int bulletHP;
		obj1->GetHP(&bulletHP);

		int monsterHP;
		obj2->GetHP(&monsterHP);

		int resultMosnterHP = monsterHP - bulletHP;
		int resultBulletHP = 0;

		obj1->SetHP(resultBulletHP);
		obj2->SetHP(resultMosnterHP);
		m_Sound->PlaySound(m_SoundShootingCollision, false, 1.0f);
	}

	if (kindObj2 == KIND_BULLET && kindObj1 == KIND_MONSTER) // Object1 : Building and Object2 : Bullet
	{
		int bulletHP;
		obj1->GetHP(&bulletHP);

		int monsterHP;
		obj2->GetHP(&monsterHP);

		int resultMosnterHP = monsterHP - bulletHP;
		int resultBulletHP = 0;

		obj1->SetHP(resultBulletHP);
		obj2->SetHP(resultMosnterHP);
		m_Sound->PlaySound(m_SoundShootingCollision, false, 1.0f);
	}

	if (kindObj1 == KIND_HERO && kindObj2 == KIND_MONSTER) // Object1 : Hero and Object2 : Building
	{
		int issacHP;
		obj1->GetHP(&issacHP);
		if (issacHP > 0 && ISSAC_DAMAGED == false)
		{
			ISSAC_DAMAGED = true;
			int resultIssacHP = issacHP - 1;
			obj1->SetHP(resultIssacHP);
		}
	}

	if (kindObj2 == KIND_HERO && kindObj1 == KIND_MONSTER) // Object1 : Building and Object2 : Hero
	{
		int issacHP;
		obj2->GetHP(&issacHP);
		if (issacHP > 0 && ISSAC_DAMAGED == false)
		{
			ISSAC_DAMAGED = true;
			int resultIssacHP = issacHP - 1;
			obj2->SetHP(resultIssacHP);
		}
	}

	if (kindObj1 == KIND_HERO && kindObj2 == KIND_MOSTER_BULLET) // Object1 : Hero and Object2 : Building
	{
		int issacHP;
		obj1->GetHP(&issacHP);
		if (issacHP > 0 && ISSAC_DAMAGED == false)
		{
			ISSAC_DAMAGED = true;
			int resultIssacHP = issacHP - 1;
			obj1->SetHP(resultIssacHP);
		}
	}

	if (kindObj2 == KIND_HERO && kindObj1 == KIND_MOSTER_BULLET) // Object1 : Building and Object2 : Hero
	{
		int issacHP;
		obj2->GetHP(&issacHP);
		if (issacHP > 0 && ISSAC_DAMAGED == false)
		{
			ISSAC_DAMAGED = true;
			int resultIssacHP = issacHP - 1;
			obj2->SetHP(resultIssacHP);
		}
	}
}


void CPlayManager::DoGarbageCollect()
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
			continue;

		int kind;
		m_Objects[i]->GetKind(&kind);

		// hp == 0 kill
		int hp = 0;
		m_Objects[i]->GetHP(&hp);
		if (hp <= 0)
		{
			if (kind == KIND_MONSTER)
			{
				m_Sound->PlaySound(m_SoundMonsterDeath, false, 1.0f);
				*m_GameState = 3;
			}
			if(kind == KIND_HERO)
				*m_GameState = 2;
			DeleteObject(i);
			continue;
		}

		// Check out of range
		float px, py, pz;
		m_Objects[i]->GetPosition(&px, &py, &pz);
		if (px > BOUNDARY_RIGHT || px < BOUNDARY_LEFT || py > BOUNDARY_UP || py < BOUNDARY_DOWN)
		{
			int kind;
			m_Objects[i]->GetKind(&kind);
			if (kind == KIND_BULLET)
			{
				DeleteObject(i);
				continue;
			}
		}

		// Check Velocity
		float vx, vy, vz;
		m_Objects[i]->GetVelocity(&vx, &vy, &vz);
		float mag = sqrtf(vx * vx + vy * vy + vz * vz);
		if (mag < FLT_EPSILON && kind == KIND_BULLET)
		{
			DeleteObject(i);
			continue;
		}
	}
}


void CPlayManager::SetGameState(int* state)
{
	m_GameState = state;
}

void CPlayManager::ManagerDefault()
{
	for (int index = 0; index < MAX_OBJECTS; index++)
		DeleteObject(index);

	// Initiate Object Array.
	for (int index = 0; index < MAX_OBJECTS; index++)
		m_Objects[index] = NULL;

	// Initiate Hero.
	m_Objects[HERO_ID] = new CObject();
	m_Objects[HERO_ID]->SetPostion(0.0f, 0.0f, 0.0f);
	m_Objects[HERO_ID]->SetVelocity(0.0f, 0.0f, 0.0f);
	m_Objects[HERO_ID]->SetAcceleration(0.0f, 0.0f, 0.0f);
	m_Objects[HERO_ID]->SetSize(1.0f, 1.0f, 1.0f);
	m_Objects[HERO_ID]->SetMass(1.0f);
	m_Objects[HERO_ID]->SetFrictionCeof(0.3f);
	m_Objects[HERO_ID]->SetColor(1.0f, 1.0f, 1.0f, 1.0f);
	m_Objects[HERO_ID]->SetKind(KIND_HERO);
	m_Objects[HERO_ID]->SetHP(6);
	m_Objects[HERO_ID]->SetState(STATE_GROUND);

	AddObject(3.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, ((float)rand() / RAND_MAX) * 2.0f + 0.6f, ((float)rand() / RAND_MAX) * 2.0f + 0.6f, 0.0f,
		KIND_MONSTER, 500000, STATE_GROUND); // Initiate Moster Object.
}
