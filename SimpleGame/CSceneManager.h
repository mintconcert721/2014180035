#pragma once

#include "Renderer.h"
#include "CObject.h"
#include "Global.h"
#include "Sound.h"


class CPlayManager
{
private:
	int* m_GameState;
	CObject *m_Objects[MAX_OBJECTS];

	Renderer *m_Renderer = NULL;
	Sound* m_Sound = NULL;

	// Texture Image
	GLuint m_Texture;
	GLuint m_AttackTexture;
	GLuint m_MonsterTexture;
	GLuint m_BackgroundTexture;
	GLuint m_MonsterHPTexture;
	GLuint m_IssacHPTexture;

	// Sound List
	int m_SoundBG{ 0 };
	int m_SoundShooting{ 0 };
	int m_SoundShootingCollision{ 0 };
	int m_SoundMonsterDeath{ 0 };

public:
	void RenderScene(float elapsedTime);
	void Update(float elapsedTime);

	void AddObject(float positionX, float positionY, float positionZ, float sizeX, float sizeY, float sizeZ, float velocityX, float velocityY, float velocityZ, int kind, int hp, int state);
	void DeleteObject(size_t index);

	void ApplyForce(float x, float y, float z, float elapsedTime);
	void Shoot(int shootDirection);

	int FindEmptySlot();

	// Collision
	void UpdateCollision();
	bool RRCollision(float minX1, float minY1, float maxX1, float maxY1, float minX2, float minY2, float maxX2, float maxY2);
	bool BBCollision(float minX1, float minY1, float minZ1, float maxX1, float maxY1, float maxZ1, float minX2, float minY2, float minZ2, float maxX2, float maxY2, float maxZ2);
	void ProcessCollision(int i, int j);
	void DoGarbageCollect();

	// state ��������
	void SetGameState(int* state);
	void ManagerDefault();

public:
	CPlayManager();
	~CPlayManager();
};

