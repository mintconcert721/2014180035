#pragma once

// RELATED TO OBJECT
#define HERO_ID 0
#define MAX_OBJECTS 1000 // OROGIN : 300

// SHOOT STATUS
#define SHOOT_NONE 0
#define SHOOT_UP 1
#define SHOOT_DOWN 2
#define SHOOT_LEFT 3
#define SHOOT_RIGHT 4

// KIND
#define KIND_HERO 0
#define KIND_BULLET 1
#define KIND_MONSTER 2
#define KIND_MOSTER_BULLET 3

// STATE
#define STATE_AIR 0 // ON THE SKY
#define STATE_GROUND 1 // ON THE FLOOR

// PHYSICS
#define GRAVITY 9.8f

// BACKGROUND
#define WINDOW_WIDTH 1000
#define WINDOW_HEIGHT 600

// HERO
#define HERO_PNG_WIDTH 100 * 0.01f
#define HERO_PNG_HEIGHT 100 * 0.01f

// MONSTER
#define MONSTER_PNG_WIDTH 50 * 0.01f
#define MONSTER_PNG_HEIGHT 50 * 0.01f

// TEARS
#define TEAR_PNG_WIDTH 50 * 0.01f
#define TEAR_PNG_HEIGHT 50 * 0.01f

// BOUNDARY
#define BOUNDARY_LEFT WINDOW_WIDTH * 0.01f * 0.5f * -1.0f
#define BOUNDARY_RIGHT WINDOW_WIDTH * 0.01f * 0.5f
#define BOUNDARY_DOWN WINDOW_HEIGHT * 0.01f * 0.5f * -1.0f
#define BOUNDARY_UP WINDOW_HEIGHT * 0.01f * 0.5f

// TEXTURE
#define ISAAC_IMAGE "./Textures/ISAAC.png"
#define TEAR_IMAGE "./Textures/TEAR.png"
#define MONSTER_IMAGE "./Textures/MONSTER.png"
#define BACKGROUND_IMAGE "./Textures/BACKGROUND.png"
#define MOSTER_HEALTHBAR_IMAGE "./Textures/MONSTER_HP.png"
#define ISSAC_HEALTHBAR_IMAGE "./Textures/ISSAC_HP.png"
#define START_IMAGE "./Textures/START.png"
#define DETERMINE_IMAGE "./Textures/CHOICE.png"
#define FINISH_IMAGE "./Textures/FINISH.png"

// SOUND
#define BACKGROUND_SOUND "./Sound/BackgroundMusic.wav"
#define SHOOTING_SOUND "./Sound/TearFire.wav"
#define COLLISION_SOUND "./Sound/TearImpact.mp3"
#define MONSTER_DEATH_SOUND "./Sound/MonsterDeath.mp3"