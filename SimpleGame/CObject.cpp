#include "stdafx.h"
#include "CObject.h"
#include <math.h>
#include <float.h>


CObject::CObject() // Constructor
{
}


CObject::~CObject() // Destructor
{
}


void CObject::SetPostion(float x, float y, float  z)
{
	m_PositionX = x;
	m_PositionY = y;
	m_PositionZ = m_HeightTest = z;
}


void CObject::SetSize(float x, float y, float z)
{
	m_SizeX = x;
	m_SizeY = y;
	m_SizeZ = z;
}


void CObject::SetMass(float mass)
{
	m_Mass = mass;
}


void CObject::SetColor(float r, float g, float b, float a)
{
	m_ColorR = r;
	m_ColorG = g;
	m_ColorB = b;
	m_ColorA = a;
}


void CObject::SetVelocity(float x, float y, float z)
{
	m_VelocityX = x;
	m_VelocityY = y;
	m_VelocityZ = z;
}


void CObject::SetAcceleration(float x, float y, float z)
{
	m_AccelerationX = x;
	m_AccelerationY = y;
	m_AccelerationZ = z;
}


void CObject::SetFrictionCeof(float friction)
{
	m_FrictionCeof = friction;
}


void CObject::SetKind(int kind)
{
	m_Kind = kind;
}


// Location to be updated = Previous Speed + Speed * Time
// Speed to be updated = Previous Speed + Acceleration * Time
// Acceleration = Force / Mass
// Force : Setting the Direction of wasd-based Force

// Speed : m/s
// Acceleration : m/s^2
// Force : N
// Mass : kg

// ApplyForce(float _x, float _y, float _time)
// �� Function called per user input
// �� Turn Force in N units
// �� Apply Accelerations to the object based on the Force and Time passed


void CObject::Update(float time)
{
	// Check Velocity
	float velocity = sqrtf((m_VelocityX * m_VelocityX) + (m_VelocityY * m_VelocityY) + (m_VelocityZ * m_VelocityZ));

	if (velocity < FLT_EPSILON)
	{
		m_VelocityX = 0.0f;
		m_VelocityY = 0.0f;
		m_VelocityZ = 0.0f;
	}
	else
	{
		if (m_State == STATE_GROUND)
		{
			//////////////////////// Calculate Friction ////////////////////////
			// Gravity Force
			float gZ;
			gZ = GRAVITY * m_Mass; // VerticalDrag

			float friction{ m_FrictionCeof * gZ };
			// friction = m_FrictionCeof * gZ; // Friction Force

			// Friction
			float fx{ -friction * m_VelocityX / velocity }, fy{ -friction * m_VelocityY / velocity };
			// fx = -friction * m_VelocityX / velocity;
			// fy = -friction * m_VelocityY / velocity;

			// Calculate Acceleration
			float accx{ fx / m_Mass }, accy{ fy / m_Mass };
			// accx = fx / m_Mass;
			// accy = fy / m_Mass;

			float afterVelX{ m_VelocityX + time * accx }, afterVelY{ m_VelocityY + time * accy };
			// float afterVelX = m_VelocityX + time * accx;
			// float afterVelY = m_VelocityY + time * accy;

			if (afterVelX * m_VelocityX < 0.0f)
			{
				m_VelocityX = 0.0f;
			}
			else
			{
				m_VelocityX = afterVelX;
			}

			if (afterVelY * m_VelocityY < 0.0f)
			{
				m_VelocityY = 0.0f;
			}
			else
			{
				m_VelocityY = afterVelY;
			}
		}
		else if (m_State == STATE_AIR)
			m_VelocityZ += 0.5f * (-GRAVITY) * time;
	}

	// Calculate Velocity
	m_VelocityX += m_AccelerationX * time;
	m_VelocityY += m_AccelerationY * time;
	m_VelocityZ += m_AccelerationZ * time;

	// Calculate Position
	m_PositionX += time * m_VelocityX;
	m_PositionY += time * m_VelocityY;
	m_PositionZ += time * m_VelocityZ;

	if (m_PositionZ > 0.0f)
	{
		m_State = STATE_AIR;
	}
	else
	{
		m_State = STATE_GROUND;
		m_PositionZ = 0.0f;
		m_VelocityZ = 0.0f;
	}
}


void CObject::ApplyForce(float x, float y, float z, float time)
{
	// Calculate Acceleration
	m_AccelerationX = x / m_Mass;
	m_AccelerationY = y / m_Mass;
	m_AccelerationZ = z / m_Mass;

	m_VelocityX += m_AccelerationX * time;
	m_VelocityY += m_AccelerationY * time;
	m_VelocityZ += m_AccelerationZ * 0.1f; // time;

	m_AccelerationX = 0.0f;
	m_AccelerationY = 0.0f;
	m_AccelerationZ = 0.0f;
}


void CObject::GetPosition(float* x, float* y, float* z)
{
	*x = this->m_PositionX;
	*y = this->m_PositionY;
	*z = this->m_PositionZ;
}


void CObject::GetSize(float* x, float* y, float* z)
{
	*x = m_SizeX;
	*y = m_SizeY;
	*z = m_SizeZ;
}


void CObject::GetMass(float* mass)
{
	*mass = m_Mass;
}


void CObject::GetColor(float* r, float* g, float* b, float* a)
{
	*r = m_ColorR;
	*g = m_ColorG;
	*b = m_ColorB;
	*a = m_ColorA;
}


void CObject::GetVelocity(float* x, float* y, float* z)
{
	*x = m_VelocityX;
	*y = m_VelocityY;
	*z = m_VelocityZ;
}


void CObject::GetAcceleration(float* accelerationX, float* accelerationY, float* accelerationZ)
{
	*accelerationX = m_AccelerationX;
	*accelerationY = m_AccelerationY;
	*accelerationZ = m_AccelerationZ;
}


void CObject::GetFrictionCeof(float* frictionCeof)
{
	*frictionCeof = m_FrictionCeof;
}


void CObject::GetKind(int* kind)
{
	*kind = m_Kind;
}


void CObject::SetHP(int hp)
{
	m_HP = hp;
}


void CObject::GetHP(int* hp)
{
	*hp = m_HP;
}


void CObject::SetState(int state)
{
	m_State = state;
}


void CObject::GetState(int* state)
{
	*state = m_State;
}