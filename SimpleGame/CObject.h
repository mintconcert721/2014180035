#pragma once

#include "Global.h"

class CObject
{
private:
	// 위치
	float m_PositionX;
	float m_PositionY;
	float m_PositionZ;

	// 크기
	float m_SizeX;
	float m_SizeY;
	float m_SizeZ;

	// 색깔
	float m_ColorR;
	float m_ColorG;
	float m_ColorB;
	float m_ColorA;

	// 속도
	float m_VelocityX;
	float m_VelocityY;
	float m_VelocityZ;

	// 가속도
	float m_AccelerationX;
	float m_AccelerationY;
	float m_AccelerationZ;

	float m_Mass;	// 질량
	float m_FrictionCeof; // 마찰력

	float m_LifeAfterConstruct = 0.0f;
	float m_HeightTest;

	// 오브젝트의 종류
	int m_Kind;

	// 능력치
	int m_HP;

	// 상태
	int m_State;

public:
	void SetPostion(float x, float y, float height);
	void SetSize(float x, float y, float z);
	void SetMass(float mass);
	void SetColor(float r, float g, float b, float a);
	void SetVelocity(float x, float y, float z);
	void SetAcceleration(float x, float y, float z);
	void Update(float time);
	void ApplyForce(float x, float y, float z, float time);
	void SetFrictionCeof(float friction);
	void SetKind(int kind);
	void SetHP(int hp);
	void SetState(int state);

public:
	void GetPosition(float*, float*, float*);
	void GetSize(float*, float*, float*);
	void GetMass(float*);
	void GetColor(float*, float*, float*, float*);
	void GetVelocity(float*, float*, float*);
	void GetAcceleration(float*, float*, float*);
	void GetFrictionCeof(float*);
	void GetKind(int*);
	void GetHP(int *hp);
	void GetState(int* state);

public:
	CObject();
	~CObject();
};

