#version 330

layout(location=0) out vec4 FragColor;

in vec2 v_TexPos;
uniform vec4 u_Color;
uniform float u_Depth;
uniform sampler2D u_Texture;

void main()
{
	vec2 newTexPos = vec2(v_TexPos.x, 1-v_TexPos.y);
	FragColor = texture(u_Texture, newTexPos)*u_Color;
    if(abs(FragColor.a) < 0.00001)
        gl_FragDepth = 1.0;
    else
        gl_FragDepth = u_Depth;
}
